# wavgat-uno-r3-vfo

Arduino project of VFO adapted for the Arduino clone Wavgat Uno R3.

## License:
This software is under the [BSD license](LICENSE.md)

## Bill of materials:
- i2c Oled display 0.96"
- DDS AD9850
![DDS AD9850](img/AD9850.png)
- rotary encoder with click button
- Arduino UNO or here a Wavgat Uno R3
![Arduino UNO](img/Arduino_UNO.png)

Pay attention that the Wagat Uno R3 doesn't work with Adafruit GFX library > 1.3.6
