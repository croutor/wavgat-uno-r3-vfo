/*******************************************************************************************************************************************
Copyright (c) 1998, Vincent Hervieux vincent.hervieux@gmail.com
All rights reserved.
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
* Neither the name of the author Vincent Hervieux, nor the
  names of its contributors may be used to endorse or promote products
  derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE REGENTS AND CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************************************************************************/
#ifndef VFO_H
#define VFO_H

#include <Arduino.h>
#include <avr/pgmspace.h>
/* Commonly used program enumerations and structures goes here */
#define SSD1306_128_64
//#define DEBUG
/// Enumerations
/* Aim to describe the cursor position on the screen */
enum {
    CURSOR_POS_NONE = 0,
    CURSOR_POS_FREQ_DIG1 = 1,
    CURSOR_POS_FREQ_DIG2,
    CURSOR_POS_FREQ_DIG3,
    CURSOR_POS_FREQ_DIG4,
    CURSOR_POS_FREQ_DIG5,
    CURSOR_POS_FREQ_DIG6,
    CURSOR_POS_FREQ_DIG7,
    CURSOR_POS_MAX = CURSOR_POS_FREQ_DIG7
};


/// Structures
typedef struct {
    uint32_t frequency; /* generator frequency */
    uint32_t freq_step; /* up/down frequency step */
    int cursor_pos; /* cursor position on display */
} context_t;

#endif /* VFO_H */

/*******************************************************************************************************************************************/

#ifndef DDS_AD9850_H
#define DDS_AD9850_H

/* inspired from http://www.vwlowen.co.uk/arduino/AD9850-waveform-generator/AD9850-waveform-generator.htm */

class DDS
{
    public:
        /* Constructor and initiliaze DDS */
        DDS();
        /* De-initiliaze and release DDS */
        ~DDS();
        /* Update generator frequency */
        void setFreq(uint32_t _frequency);

        static const uint32_t MAX_FREQ =  60000000;
        static const uint32_t MIN_FREQ =         1;
    private:
        /**
         * CONNEXIONS
         * GND    GND
         * VDD    +5V
         * W_CLK  pin 8
         * FQ_UD  pin 7
         * DATA   pin 6
         * RESET  pin 5
         * 
         */
        /* AD9850 Module pins. */
        const static int W_CLK PROGMEM = 8;
        const static int FQ_UD PROGMEM = 7;
        const static int DATA PROGMEM = 6;
        const static int RESET PROGMEM = 5;
        /*  */
        inline void pulse(int _pin){digitalWrite(_pin, HIGH); digitalWrite(_pin, LOW);};
        void init();
        /* delayed initialisation */
        boolean initialized_;
};

#endif /* DDS_AD9850_H */
/*******************************************************************************************************************************************/
#ifndef DDS_H
#define DDS_H

/* Header to swich between the 2 supported DDS*/

#endif // DDS_H
/*******************************************************************************************************************************************/
#ifndef DISPLAY_H
#define DISPLAY_H

#include <Adafruit_SSD1306.h>
#include <Adafruit_GFX.h>

class Display
{
    public:
        /* Constructor and initiliaze display */
        Display();
        /* De-initiliaze and release display */
        ~Display();
        /* Update info on display */
        void update(uint32_t _frequency, int _position);
    private:
        /**
         * CONNEXIONS
         * GND    GND
         * VDD    +5V
         * SCLK   pin A5
         * SDA    pin A4 
         */
        /* RF ranges enum */
        enum {
                RANGE_RF_UNKNOWN = -1,
                RANGE_RF_LF = 0,
                RANGE_RF_MF,
                RANGE_RF_HF,
                RANGE_RF_VHF,
                RANGE_RF_MAX = RANGE_RF_VHF
        };
        /* HAM band enum */     
        enum {
                BAND_UNKNOWN = -1,
                BAND_2200 = 0,
                BAND_1750,
                BAND_630,
                BAND_160,
                BAND_80,
                BAND_60,
                BAND_40,
                BAND_30,
                BAND_20,
                BAND_17,
                BAND_15,
                BAND_12,
                BAND_11,
                BAND_10,
                BAND_6,
                BAND_2,
                BAND_MAX = BAND_2
        };
       
        /* definning a structure to store RF ranges and bands */
        typedef struct {
                uint32_t low_bound; /* low boundary frequency range */
                uint32_t high_bound; /* high boundary frequency range */
                int index; /* range or band index */
        } range_t;

        const static int16_t FREQ_BIG_TEXT_POS_X PROGMEM = 1;
        const static int16_t FREQ_SMALL_TEXT_POS_X PROGMEM = 44;
        const static int16_t FREQ_TEXT_POS_Y PROGMEM = 20;
        /* Display initialisation */
        void init();
        /* Update displayed frequency */
        void setFreq(uint32_t _frequency, uint16_t _color);
        /* Update displayed cursor position */
        void setCursorPos(int _position, uint16_t _color);
        /* Update displayed RF range */
        void setRFRange(int _rf_range, uint16_t _color);
        /* Update displayed RF band */
        void setBand(int _band, uint16_t _color);

        /* @brief get RF range index 
         * @param: frequency
         * @return RF range index in  -1 if none matching */
        int getRFRange(uint32_t _frequency);
        /* @brief get band index 
         * @param: frequency
         * @return RF range index in  -1 if none matching */
        int getBand(uint32_t _frequency);

        // OLED module
        Adafruit_SSD1306 display_;
        // current displayed values
        bool first_display_;
        /* display context */
        uint32_t current_freq_;
        int current_pos_;
        int current_rf_band_;
        int current_rf_range_;

};

#endif /* DISPLAY_H */
/*******************************************************************************************************************************************/

////
//// DDS: Direct Digital Synthesis
//// using AD9850
////

DDS::DDS():
initialized_(false)
{
  pinMode(FQ_UD, OUTPUT); /* Configure pins for output to AD9850 module. */
  pinMode(W_CLK, OUTPUT);
  pinMode(DATA, OUTPUT);
  pinMode(RESET, OUTPUT);
}

DDS::~DDS()
{

}

void DDS::init()
{
  /* Initialise the AD9850 module. */
  pulse(RESET);
  pulse(W_CLK);
  pulse(FQ_UD); /* this pulse enables serial mode - Datasheet page 12 figure 10 */
}


void DDS::setFreq(uint32_t _frequency) 
{
  const static double AD9850_CLOCK PROGMEM = 125000000;
  const static double AD9850_MAX_REGISTER PROGMEM = 0xFFFFFFFF;
  int32_t freq1 = (double)_frequency * AD9850_MAX_REGISTER/AD9850_CLOCK; /* note 125 MHz clock on 9850 */
  if(!initialized_)
  {
      init();
      initialized_ = true;
  }
  for (uint8_t b = 0; b < 4; b++, freq1 >>= 8) {
    shiftOut(DATA, W_CLK, LSBFIRST, freq1 & 0xFF);
  }
  shiftOut(DATA, W_CLK, LSBFIRST, 0x00);
  pulse(FQ_UD); /* Done!  Should see output */
}

////
//// Display: using an OLED SSD1306
////
#include <Fonts/FreeSansBold12pt7b.h>
#include <Fonts/FreeSansBold9pt7b.h>
#include <Wire.h>

#define SCREEN_HEIGHT 64
#define SCREEN_WIDTH 128

#define OLED_RESET -1 // -1 = no reset pin

Display::Display():
display_(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET),
first_display_(true),
current_freq_(0),
current_pos_(0),
current_rf_band_(BAND_UNKNOWN),
current_rf_range_(RANGE_RF_UNKNOWN)
{

}

Display::~Display()
{
    display_.clearDisplay();
}

void Display::init()
{
    const static int16_t FREQ_DOT_TEXT_POS_X PROGMEM = 40;
    const static int16_t MHZ_TEXT_POS_X PROGMEM = 102;
    const static byte I2C_OLED_ADDRESS_ PROGMEM = 0x3c;
    // initialize the Display I2C address (for the 128x64)
    display_.begin(SSD1306_SWITCHCAPVCC, I2C_OLED_ADDRESS_);
    display_.clearDisplay();
    /* Top and bottom lines */
    display_.drawFastHLine(0, 0, SSD1306_LCDWIDTH, WHITE);
    display_.drawFastHLine(0, 31, SSD1306_LCDWIDTH, WHITE);
    display_.drawFastHLine(0, 40, SSD1306_LCDWIDTH, WHITE);
    display_.drawFastHLine(0, SSD1306_LCDHEIGHT - 1, SSD1306_LCDWIDTH, WHITE);
    /* Frequency dot */
    display_.setTextColor(WHITE);
    display_.setFont(&FreeSansBold9pt7b);
    display_.setCursor(FREQ_DOT_TEXT_POS_X,FREQ_TEXT_POS_Y);
    display_.print(F("."));
    /* Mhz sign */
    display_.setCursor(MHZ_TEXT_POS_X,FREQ_TEXT_POS_Y);
    display_.setFont();
    display_.print(F(" MHz"));
}

void Display::setFreq(uint32_t _freq, uint16_t _color) 
{
    const static uint16_t MAX_FREQ_STR_LENGTH PROGMEM = 9; /* 160000000*/
    char str[MAX_FREQ_STR_LENGTH + 1] = {0};
    char big_str[4] = {0};
    snprintf(str, MAX_FREQ_STR_LENGTH + 1, "%09lu", _freq);
    strncpy (big_str, str, 3 * sizeof(char));
    display_.setFont(&FreeSansBold12pt7b);
    display_.setCursor(FREQ_BIG_TEXT_POS_X,FREQ_TEXT_POS_Y);
    display_.setTextColor(_color);
    display_.print(big_str);
    display_.setCursor(FREQ_SMALL_TEXT_POS_X,FREQ_TEXT_POS_Y);
    display_.setFont(&FreeSansBold9pt7b);
    display_.print(str + 3 * sizeof(char));
}

void Display::setCursorPos(int _pos, uint16_t _color)
{
    const static int16_t CURSOR_BIG_TEXT_POS_X PROGMEM = FREQ_BIG_TEXT_POS_X + 27;
    const static int16_t CURSOR_BIG_WIDTH PROGMEM = 10;
    const static int16_t CURSOR_SMALL_TEXT_POS_X PROGMEM = FREQ_BIG_TEXT_POS_X + 45;
    const static int16_t CURSOR_SMALL_WIDTH PROGMEM = 8;
    const static int16_t CURSOR_TEXT_POS_Y PROGMEM = 24;
    int16_t x, length;
    bool draw = false;
    switch(_pos)
    {
        case CURSOR_POS_FREQ_DIG7:
            length = CURSOR_BIG_WIDTH;
            x = CURSOR_BIG_TEXT_POS_X;
            draw = true;
        break;
        case CURSOR_POS_FREQ_DIG6:
        case CURSOR_POS_FREQ_DIG5:
        case CURSOR_POS_FREQ_DIG4:
        case CURSOR_POS_FREQ_DIG3:
        case CURSOR_POS_FREQ_DIG2:
        case CURSOR_POS_FREQ_DIG1:
            length = CURSOR_SMALL_WIDTH;
            x = CURSOR_SMALL_TEXT_POS_X +
                (CURSOR_POS_FREQ_DIG6 - _pos) * (length + 2);
            draw = true;
        break;
    }
    if(draw)
        display_.drawRect(x, CURSOR_TEXT_POS_Y, length, 2, _color);
}

void Display::setRFRange(int _rf_range, uint16_t _color)
{
    const static char LF[]  PROGMEM = "LF";
    const static char MF[]  PROGMEM = "MF";
    const static char HF[]  PROGMEM = "HF";
    const static char VHF[] PROGMEM = "VHF";
    const static char* const rf_ranges_names[] PROGMEM = {
        LF, MF, HF, VHF
    };
    const static int16_t RANGE_TEXT_POS_X PROGMEM = 85;
    const static int16_t RANGE_TEXT_POS_Y PROGMEM = 50;
    char str[4] = {0};
    if(_rf_range > RANGE_RF_UNKNOWN && _rf_range <= RANGE_RF_MAX)
    {
        display_.setTextSize(1);
        display_.setFont();
        display_.setCursor(RANGE_TEXT_POS_X,RANGE_TEXT_POS_Y);
        display_.setTextColor(_color);
        strcpy_P(str, (char*)pgm_read_word(&(rf_ranges_names[_rf_range])));
        display_.print(str);  
    }
}

void Display::setBand(int _band, uint16_t _color)
{
    const static uint16_t rf_bands[] PROGMEM = {
        2200, 1750, 630, 160, 80, 60, 40, 30, 20, 17, 15, 12, 11, 10, 6, 2 
    };
    const static int16_t BAND_TEXT_POS_X PROGMEM = 5;
    const static int16_t BAND_TEXT_POS_Y PROGMEM = 50;
    char rf_band[6] = {0};
    if(_band > BAND_UNKNOWN && _band <= BAND_MAX)
    {
        display_.setTextSize(1);
        display_.setFont();
        display_.setCursor(BAND_TEXT_POS_X,BAND_TEXT_POS_Y);
        display_.setTextColor(_color);
        snprintf(rf_band, 5, "%um", pgm_read_word_near(rf_bands + _band));
        display_.print(rf_band);
    }
}

void Display::update(uint32_t _frequency, int _position)
{
    bool update = false;
    int rf_range = RANGE_RF_UNKNOWN;
    int rf_band = BAND_UNKNOWN;
    if(first_display_)
    {
        init();
        /* Never initialize again */
        first_display_ = false;
    }
    if(_frequency != current_freq_)
    {
        /* TODO: could be optimized to update only the impacted digits */
        setFreq(current_freq_, BLACK);
        setFreq(_frequency, WHITE);
        current_freq_ = _frequency;
        update = true;
        /* recompute RF range, bands*/
        rf_range = getRFRange(_frequency);
        rf_band = getBand(_frequency);
        /* update diplay if needed */
        if(rf_range != current_rf_range_)
        {
            setRFRange(current_rf_range_, BLACK);
            setRFRange(rf_range, WHITE);
            current_rf_range_ = rf_range;
        }
        if(rf_band != current_rf_band_)
        {
            setBand(current_rf_band_, BLACK);
            setBand(rf_band, WHITE);
            current_rf_band_ = rf_band;
        }
    }
    if(_position != current_pos_)
    {
        setCursorPos(current_pos_, BLACK);
        setCursorPos(_position, WHITE);
        current_pos_ = _position;
        update = true;
    }
    if(update)
    {
        display_.display();
    }
}


int Display::getRFRange(uint32_t _frequency)
{
    /*
     * Frequency bands according to
     * https://en.wikipedia.org/wiki/Radio_frequency#Frequency_bands
     */
    const static uint32_t LF_LOW   PROGMEM =     30000;
    const static uint32_t MF_LOW   PROGMEM =    300000;
    const static uint32_t HF_LOW   PROGMEM =   3000000;
    const static uint32_t VHF_LOW  PROGMEM =  30000000;
    const static uint32_t VHF_HIGH PROGMEM = 300000000;
    if     (_frequency >  HF_LOW && _frequency <  VHF_LOW) return RANGE_RF_HF;
    else if(_frequency > VHF_LOW && _frequency < VHF_HIGH) return RANGE_RF_VHF;
    else if(_frequency >  MF_LOW && _frequency <   HF_LOW) return RANGE_RF_MF;
    else if(_frequency >  LF_LOW && _frequency <   MF_LOW) return RANGE_RF_LF;
    else return  RANGE_RF_UNKNOWN;
}

int Display::getBand(uint32_t _frequency)
{   
    /* 
     * Display band names according to
     * https://en.wikipedia.org/wiki/Amateur_radio_frequency_allocations
     */
    const static uint32_t BAND_2200_LOW PROGMEM =    135700;
    const static uint32_t BAND_2200_HI  PROGMEM =    135800;
    const static uint32_t BAND_1750_LOW PROGMEM =    160000;
    const static uint32_t BAND_1750_HI  PROGMEM =    190000;
    const static uint32_t BAND_630_LOW  PROGMEM =    472000;
    const static uint32_t BAND_630_HI   PROGMEM =    479000;
    const static uint32_t BAND_160_LOW  PROGMEM =   1800000;
    const static uint32_t BAND_160_HI   PROGMEM =   2000000;
    const static uint32_t BAND_80_LOW   PROGMEM =   3500000;
    const static uint32_t BAND_80_HI    PROGMEM =   4000000;
    const static uint32_t BAND_60_LOW   PROGMEM =   5332000;
    const static uint32_t BAND_60_HI    PROGMEM =   5450000;
    const static uint32_t BAND_40_LOW   PROGMEM =   7000000;
    const static uint32_t BAND_40_HI    PROGMEM =   7300000;
    const static uint32_t BAND_30_LOW   PROGMEM =  10100000;
    const static uint32_t BAND_30_HI    PROGMEM =  10150000;
    const static uint32_t BAND_20_LOW   PROGMEM =  14000000;
    const static uint32_t BAND_20_HI    PROGMEM =  14350000;
    const static uint32_t BAND_17_LOW   PROGMEM =  18068000;
    const static uint32_t BAND_17_HI    PROGMEM =  18168000;
    const static uint32_t BAND_15_LOW   PROGMEM =  21000000;
    const static uint32_t BAND_15_HI    PROGMEM =  21450000;
    const static uint32_t BAND_12_LOW   PROGMEM =  24890000;
    const static uint32_t BAND_12_HI    PROGMEM =  24990000;
    const static uint32_t BAND_11_LOW   PROGMEM =  25000000;
    const static uint32_t BAND_11_HI    PROGMEM =  28000000;
    const static uint32_t BAND_10_LOW   PROGMEM =  28000000;
    const static uint32_t BAND_10_HI    PROGMEM =  29700000;
    const static uint32_t BAND_6_LOW    PROGMEM =  50000000;
    const static uint32_t BAND_6_HI     PROGMEM =  54000000;
    const static uint32_t BAND_2_LOW    PROGMEM = 144000000;
    const static uint32_t BAND_2_HI     PROGMEM = 148000000;
    if      (_frequency >   BAND_10_LOW && _frequency <=   BAND_10_HI) return   BAND_10;
    else if (_frequency >   BAND_11_LOW && _frequency <=   BAND_11_HI) return   BAND_11;
    else if (_frequency >   BAND_12_LOW && _frequency <=   BAND_12_HI) return   BAND_12;
    else if (_frequency >   BAND_15_LOW && _frequency <=   BAND_15_HI) return   BAND_15;
    else if (_frequency >   BAND_17_LOW && _frequency <=   BAND_17_HI) return   BAND_17;
    else if (_frequency >   BAND_20_LOW && _frequency <=   BAND_20_HI) return   BAND_20;
    else if (_frequency >   BAND_30_LOW && _frequency <=   BAND_30_HI) return   BAND_30;
    else if (_frequency > BAND_2200_LOW && _frequency <= BAND_2200_HI) return BAND_2200;
    else if (_frequency > BAND_1750_LOW && _frequency <= BAND_1750_HI) return BAND_1750;
    else if (_frequency >  BAND_630_LOW && _frequency <=  BAND_630_HI) return  BAND_630;
    else if (_frequency >  BAND_160_LOW && _frequency <=  BAND_160_HI) return  BAND_160;
    else if (_frequency >   BAND_80_LOW && _frequency <=   BAND_80_HI) return   BAND_80;
    else if (_frequency >   BAND_60_LOW && _frequency <=   BAND_60_HI) return   BAND_60;
    else if (_frequency >   BAND_40_LOW && _frequency <=   BAND_40_HI) return   BAND_40;
    else if (_frequency >    BAND_6_LOW && _frequency <=    BAND_6_HI) return    BAND_6;
    else if (_frequency >    BAND_2_LOW && _frequency <=    BAND_2_HI) return    BAND_2;
    else return BAND_UNKNOWN;
}
////
//// Main entry point
////
//#define ENCODER_OPTIMIZE_INTERRUPTS
#include <Encoder.h>   // https://www.pjrc.com/teensy/td_libs_Encoder.html

/// Globals:
/* Main context */
context_t context = {
    /* Wordwild 11 meters DX calling frequency */
    .frequency = 27555000,
    /* Default step is a 10kHz channel */
    .freq_step = 10000,
    .cursor_pos = CURSOR_POS_FREQ_DIG5,
};

/* Encoder pins:
 * D3 = A pin
 * D2 = B pin
 * D4 = button pin
 */
Encoder encoder(2, 3);
Display display;
DDS dds;

/// Arduino setup
void setup()
{
    pinMode(4, INPUT_PULLUP);  /* select button */
#ifdef DEBUG
    Serial.begin(9600);
    Serial.println(F("Setting up display"));
#endif
    /**
     * CONNEXIONS
     * GND    GND
     * VDD    +5V
     * A      D2
     * B      D3
     * BTN    D4 
     */
#ifdef DEBUG
    Serial.println(F("Setting up timer"));
#endif
    /* start the generator */
#ifdef DEBUG
    Serial.println(F("Setting up DDS"));
#endif

    display.update(context.frequency, context.cursor_pos);
    dds.setFreq(context.frequency);
}

uint32_t quick_pow10(int n)
{
    const static uint32_t pow10[7] PROGMEM = {
        1, 10, 100, 1000, 10000, 
        100000, 1000000
    };
    return (uint32_t) pgm_read_dword_near(pow10 + n); 
}


/// Arduino main loop
void loop()
{
    uint32_t prev_frequency = context.frequency;
    int prev_cursor_pos = context.cursor_pos;
    int16_t increment = 0;
    if(digitalRead(4) == LOW)
    {
        context.cursor_pos++;
        if(context.cursor_pos > CURSOR_POS_MAX)
        {
            context.cursor_pos = CURSOR_POS_NONE;
            context.freq_step = 0;
        }
        else
        {
            context.freq_step = quick_pow10(context.cursor_pos-1);
        }
    }
    increment = encoder.read();
    // work around the fact that increments are not going 1 by 1
    increment = increment / 4;
    if(increment) 
    {
        encoder.write(0);
#ifdef DEBUG
        Serial.print(F("Increment: "));Serial.println(increment);
#endif
        context.frequency += ((uint32_t)increment * context.freq_step);
        /* Limits */
        if(context.frequency > DDS::MAX_FREQ)
            context.frequency = DDS::MAX_FREQ;
        else if(context.frequency < DDS::MIN_FREQ)
            context.frequency = DDS::MIN_FREQ;
    }
    if(prev_frequency != context.frequency ||
        prev_cursor_pos != context.cursor_pos)
    {
        display.update(context.frequency, context.cursor_pos);
        if(prev_frequency != context.frequency)
        {
            dds.setFreq(context.frequency);
        }
    }
}
